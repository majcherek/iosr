package pl.edu.agh.iosr.vmonitor.data.test;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.index.IndexInfo;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pl.edu.agh.iosr.vmonitor.data.stats.HypervisorTick;
import pl.edu.agh.iosr.vmonitor.data.stats.HypervisorTickRepository;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by molok on 2014-05-24.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {MongoTestConfig.class})
public class HypervisorTickRepositoryTest {

    @Autowired
    private MongoOperations mongo;

    @Autowired
    private HypervisorTickRepository hypervisorTickRepository;

    @Before
    public void setup(){
        mongo.dropCollection(HypervisorTick.class);
    }

    public boolean hasIndexOn(String ... fields) {
        List<IndexInfo> indexes = mongo.indexOps(HypervisorTick.class).getIndexInfo();
        for (IndexInfo info : indexes) {
            if (info.isIndexForFields(Arrays.asList(fields))) {
                return true;
            }
        }
        return false;
    }

    @Test
    public void simpleTest(){

        Assert.assertEquals(0, hypervisorTickRepository.count());

        hypervisorTickRepository.save(simpleItem());

        Assert.assertEquals(1, hypervisorTickRepository.count());
    }

    private HypervisorTick simpleItem() {
        HypervisorTick tick = new HypervisorTick();
        tick.setDate(new Date());
        tick.setMetricName("some name");
        tick.setValue(150.);
        tick.setUnit("MB");
        return tick;
    }

    @After
    public void cleanUp(){
        mongo.dropCollection(HypervisorTick.class);
    }

}
