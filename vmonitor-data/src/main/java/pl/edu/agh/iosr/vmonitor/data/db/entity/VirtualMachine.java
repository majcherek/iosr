package pl.edu.agh.iosr.vmonitor.data.db.entity;

import java.util.Set;

import org.neo4j.graphdb.Direction;
import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;

@NodeEntity
public class VirtualMachine {
	
	@GraphId
	private Long id;
	@Indexed
	private String name;
	@Indexed
	private String virtualboxId;
	@Indexed 
	private String os;
	@Indexed
	private String state;
	@Indexed
	private String ip;
	
	@RelatedTo(type="HAS_APPSERVER", direction=Direction.BOTH)
	private @Fetch Set<AppServer> appServers;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVirtualboxId() {
		return virtualboxId;
	}

	public void setVirtualboxId(String virtualboxId) {
		this.virtualboxId = virtualboxId;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Set<AppServer> getAppServers() {
		return appServers;
	}

	public void setAppServers(Set<AppServer> appServers) {
		this.appServers = appServers;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	@Override
	public String toString() {
		return name ;
	}	
	
	

}
