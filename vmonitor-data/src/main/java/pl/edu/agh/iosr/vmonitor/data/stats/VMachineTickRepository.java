package pl.edu.agh.iosr.vmonitor.data.stats;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public interface VMachineTickRepository extends CrudRepository<VMachineTick,ObjectId>{

    List<VMachineTick> findByMachineIdAndMetricNameAndDateBetween(String id, String metricName, Date from, Date to, Sort sort);

}
