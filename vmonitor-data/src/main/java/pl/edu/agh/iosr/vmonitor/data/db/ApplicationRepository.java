package pl.edu.agh.iosr.vmonitor.data.db;

import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.stereotype.Repository;

import pl.edu.agh.iosr.vmonitor.data.db.entity.AppServer;
import pl.edu.agh.iosr.vmonitor.data.db.entity.Application;

@Repository
public interface ApplicationRepository extends GraphRepository<Application>{
	
	Application findByAppServerAndName(AppServer appServer, String name);

}
