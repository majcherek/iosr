package pl.edu.agh.iosr.vmonitor.data.stats;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public interface HypervisorTickRepository extends MongoRepository<HypervisorTick, ObjectId> {

    List<HypervisorTick> findByHypervisorIdAndMetricName(Long id, String metricName, Sort sort);

    List<HypervisorTick> findByHypervisorIdAndMetricNameAndDateBetween(Long id, String metricName, Date from, Date to, Sort sort);

}
