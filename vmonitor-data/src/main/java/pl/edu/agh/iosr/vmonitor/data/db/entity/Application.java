package pl.edu.agh.iosr.vmonitor.data.db.entity;

import org.neo4j.graphdb.Direction;
import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;

@NodeEntity
public class Application {
	
	@GraphId
	private Long id;
	
	@Indexed
	private String name;
	
	@RelatedTo(type="DEPLOYED", direction=Direction.BOTH)
	private @Fetch AppServer appServer;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public AppServer getAppServer() {
		return appServer;
	}

	public void setAppServer(AppServer appServer) {
		this.appServer = appServer;
	}

	@Override
	public String toString() {
		return name;
	}
	
	

}
