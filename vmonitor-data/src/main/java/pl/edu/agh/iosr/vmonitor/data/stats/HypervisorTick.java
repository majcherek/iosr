package pl.edu.agh.iosr.vmonitor.data.stats;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by molok on 2014-05-24.
 */

@Document
public class HypervisorTick {

    public static final String METRIC_NAME__FIELD = "metricName";
    public static final String DATE__FIELD = "date";
    public static final String VALUE__FIELD = "value";
    public static final String UNIT__FIELD = "unit";
    public static final String HYPERVISOR_ID__FIELD = "hypervisorId";

    @Id
    private ObjectId id;

    private String metricName;
    private Date date;
    private double value;
    private String unit;
    @Indexed
    private Long hypervisorId;

    public Long getHypervisorId() {
        return hypervisorId;
    }

    public void setHypervisorId(Long hypervisorId) {
        this.hypervisorId = hypervisorId;
    }

    public ObjectId getId() {
        return id;
    }

    public String getMetricName() {
        return metricName;
    }

    public void setMetricName(String metricName) {
        this.metricName = metricName;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
