package pl.edu.agh.iosr.vmonitor.data.stats;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public interface VmStateTickRepository extends CrudRepository<VmStateTick,ObjectId>{

}
