package pl.edu.agh.iosr.vmonitor.data.db.entity;

import java.util.Set;

import org.neo4j.graphdb.Direction;
import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;

@NodeEntity
public class AppServer {
	
	@GraphId
	private Long id;
	
	@Indexed
	private String name;
		
	@Indexed
	private String jmxAdminPort;
	
	@Indexed
	private String user;
	
	@Indexed 
	private String pass;
	
	@RelatedTo(type="DEPLOYED", direction=Direction.BOTH)
	private @Fetch Set<Application> applications;
	
	@RelatedTo(type="HAS_APPSERVER", direction=Direction.BOTH)
	private @Fetch VirtualMachine virtualMachine;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getJmxAdminPort() {
		return jmxAdminPort;
	}

	public void setJmxAdminPort(String jmxAdminPort) {
		this.jmxAdminPort = jmxAdminPort;
	}

	public Set<Application> getApplications() {
		return applications;
	}

	public void setApplications(Set<Application> applications) {
		this.applications = applications;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public VirtualMachine getVirtualMachine() {
		return virtualMachine;
	}

	public void setVirtualMachine(VirtualMachine virtualMachine) {
		this.virtualMachine = virtualMachine;
	}

	@Override
	public String toString() {
		return "AppServer " + name ;
	}
	
	

}
