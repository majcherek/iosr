package pl.edu.agh.iosr.vmonitor.data.stats;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.virtualbox_4_3.MachineState;

import java.util.Date;

@Document
public class VmStateTick {

    public static final String STATE__FIELD = "state";
    public static final String MACHINE_ID__FIELD = "machineId";

    @Id
    private ObjectId id;
    private MachineState state;
    private Date date;
    @Indexed
    private String machineId;

    public MachineState getState() {
        return state;
    }

    public void setState(MachineState state) {
        this.state = state;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getMachineId() {
        return machineId;
    }

    public void setMachineId(String machineId) {
        this.machineId = machineId;
    }
}
