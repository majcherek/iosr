package pl.edu.agh.iosr.vmonitor.data.db;

import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.stereotype.Repository;

import pl.edu.agh.iosr.vmonitor.data.db.entity.VirtualMachine;

@Repository
public interface VirtualMachineRepository extends GraphRepository<VirtualMachine> {
	
	VirtualMachine findByVirtualboxId(String virtualboxId);

}
