package pl.edu.agh.iosr.vmonitor.data.db.entity;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

import org.neo4j.graphdb.Direction;
import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;

@NodeEntity
public class Hypervisor {
	
	@GraphId
	private Long id;
	
	@Indexed
	private String ip;
	@Indexed
	private String port;
	@Indexed
	private String user;
	@Indexed
	private String pass;
	
	@RelatedTo(type="HAS", direction=Direction.OUTGOING)
	private @Fetch Set<VirtualMachine> vms;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public Set<VirtualMachine> getVms() {
		return vms;
	}

	public void setVms(Set<VirtualMachine> vms) {
		this.vms = vms;
	}

	@Override
	public String toString() {
		return "Hypervisor " + id ;
	}
	
	


	
	

}
