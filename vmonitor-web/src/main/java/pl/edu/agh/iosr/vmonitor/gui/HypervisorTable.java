package pl.edu.agh.iosr.vmonitor.gui;

import java.util.Set;

import pl.edu.agh.iosr.vmonitor.data.db.entity.Hypervisor;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.Table;
import com.vaadin.ui.themes.Reindeer;

public class HypervisorTable extends Table {

    private static final long serialVersionUID = 1L;

    private BeanItemContainer<Hypervisor> container = new BeanItemContainer<Hypervisor>(Hypervisor.class);

    public HypervisorTable() {
        super();
        setContainerDataSource(container);
        setVisibleColumns(new Object[] { "id", "ip", "port"});
        setStyleName(Reindeer.TABLE_STRONG);
        setImmediate(true);
        setSizeFull();
    }

    public void fillWithData(Set<Hypervisor> nodes) {
        container.removeAllItems();
        container.addAll(nodes);
        sort(new Object[] { "id" }, new boolean[] { true });
        refreshRowCache();
    }

}
