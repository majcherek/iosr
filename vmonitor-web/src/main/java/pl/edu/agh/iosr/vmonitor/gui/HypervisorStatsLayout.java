package pl.edu.agh.iosr.vmonitor.gui;

import com.vaadin.addon.charts.model.*;
import com.vaadin.data.Property;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.HorizontalLayout;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import pl.edu.agh.iosr.vmonitor.data.db.entity.Hypervisor;
import pl.edu.agh.iosr.vmonitor.data.stats.HypervisorTick;
import pl.edu.agh.iosr.vmonitor.data.stats.HypervisorTickRepository;
import pl.edu.agh.iosr.vmonitor.vm.VMmanager;
import pl.edu.agh.iosr.vmonitor.vm.monitoring.StatisticsMonitor;

import com.vaadin.addon.charts.Chart;
import com.vaadin.ui.VerticalLayout;

import java.util.Date;
import java.util.List;

public class HypervisorStatsLayout extends VerticalLayout {

    private static final long serialVersionUID = 7630851737458863812L;
    private VerticalLayout chartContainer;
    private MongoOperations mongoOperations;
    private ComboBox metricCombo;
    private DateField fromDate;
    private DateField toDate;
    private HypervisorTickRepository hypervisorTickRepository;

    public HypervisorStatsLayout() {
    	setSpacing(true);
        initComponents();
    }

    private void initComponents() {

        HorizontalLayout metricProperties = new HorizontalLayout();
        metricProperties.setMargin(true);
        metricProperties.setSpacing(true);
        addComponent(metricProperties);

        //metric name chooser
        metricCombo = new ComboBox("Choose metric:");
        metricProperties.addComponent(metricCombo);

        //selection of date
        fromDate = new DateField("From", new Date());
        fromDate.setResolution(Resolution.MINUTE);
        toDate = new DateField("To", new Date());
        toDate.setResolution(Resolution.MINUTE);

        metricProperties.addComponent(fromDate);
        metricProperties.addComponent(toDate);

        chartContainer = new VerticalLayout();
        chartContainer.setMargin(true);
        chartContainer.setSpacing(true);
        addComponent(chartContainer);

    }

    public void setMonitoredHypervisor(final Hypervisor hypervisor) {
        Property.ValueChangeListener listener = new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                refreshChart(hypervisor.getId());
            }
        };

        metricCombo.addValueChangeListener(listener);
        toDate.addValueChangeListener(listener);
        fromDate.addValueChangeListener(listener);
    }

    private void refreshChart(Long id) {
        chartContainer.removeAllComponents();
        String metricName = (String) metricCombo.getValue();
        if (metricName != null) {
            Chart metricChart = new Chart(ChartType.LINE);
            metricChart.getConfiguration().setTitle(metricName);
            DataSeries series = new DataSeries(metricName);

            PlotOptionsLine plotOptions = new PlotOptionsLine();
            plotOptions.setMarker(new Marker(false));
            metricChart.getConfiguration().setPlotOptions(plotOptions);

            Date startDate = fromDate.getValue();
            Date endDate = toDate.getValue();

            if (hypervisorTickRepository != null) {

                List<HypervisorTick> aggResult = hypervisorTickRepository.findByHypervisorIdAndMetricNameAndDateBetween(id, metricName, startDate, endDate,
                        new Sort(Sort.Direction.ASC, HypervisorTick.DATE__FIELD));

                for (HypervisorTick tick : aggResult) {
                    series.add(new DataSeriesItem(tick.getDate(), tick.getValue()));
                }

                metricChart.getConfiguration().getxAxis().setType(AxisType.DATETIME);

                metricChart.getConfiguration().addSeries(series);
                chartContainer.addComponent(metricChart);
            }
        }
    }


    public void setMongoOperations(MongoOperations mongoOperations) {
        this.mongoOperations = mongoOperations;
        List<String> allMetricNames = mongoOperations.getCollection("hypervisorTick").distinct(HypervisorTick.METRIC_NAME__FIELD);
        for (String metricName : allMetricNames) {
            metricCombo.addItem(metricName);
        }
    }

    public MongoOperations getMongoOperations() {
        return mongoOperations;
    }

    public HypervisorTickRepository getHypervisorTickRepository() {
        return hypervisorTickRepository;
    }

    public void setHypervisorTickRepository(HypervisorTickRepository hypervisorTickRepository) {
        this.hypervisorTickRepository = hypervisorTickRepository;
    }
}
