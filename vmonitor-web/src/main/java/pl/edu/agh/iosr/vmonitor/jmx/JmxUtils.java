package pl.edu.agh.iosr.vmonitor.jmx;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

public class JmxUtils {
	
	private static final String SERVICE_PREFIX = "service:jmx:rmi:///jndi/rmi://";
	private static final String SERVICE_SUFIX = "/jmxrmi";
	private static final String APPLICATIONS_MBEAN_NAME = "amx:pp=/domain,type=applications";
	
	public static List<String> getApplications(String ip, String port) throws Exception{
		List<String> applications = new ArrayList<>();
			String service = SERVICE_PREFIX + ip + ":" + port + SERVICE_SUFIX;
			
			JMXServiceURL target = new JMXServiceURL(service);
	        Map env = new HashMap();
			String[] creds = {"admin", "misior"};
			env.put(JMXConnector.CREDENTIALS, creds);			
	        JMXConnector connector = JMXConnectorFactory.connect(target, env);
	        MBeanServerConnection serverConnection = connector.getMBeanServerConnection();
	        
	        ObjectName bean = new ObjectName(APPLICATIONS_MBEAN_NAME);
	        
	        List<ObjectName> apps = (List<ObjectName>) serverConnection.invoke(bean, "getApplications", null, null);
	        for(ObjectName app : apps){
	        	applications.add(serverConnection.getAttribute(app, "Name").toString());
	        }
	        
	        connector.close();

		
		return applications;

	}

}
