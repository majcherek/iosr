package pl.edu.agh.iosr.vmonitor.gui;

import pl.edu.agh.iosr.vmonitor.data.db.AppServerRepository;
import pl.edu.agh.iosr.vmonitor.data.db.VirtualMachineRepository;
import pl.edu.agh.iosr.vmonitor.data.db.entity.AppServer;
import pl.edu.agh.iosr.vmonitor.data.db.entity.VirtualMachine;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class AddAppServerWindow extends Window{
	
	private VirtualMachineRepository vmRepository;
	private AppServerRepository appServerRepository;
	private MonitoringView view;
	
	private VerticalLayout mainLayout = new VerticalLayout();
	 
	private TextField nameTextField = new TextField();
	private HorizontalLayout nameLayout = new HorizontalLayout();
	
	private TextField portTextField = new TextField();
	private HorizontalLayout portLayout = new HorizontalLayout();
	
	private TextField ipTextField = new TextField();
	private HorizontalLayout ipLayout = new HorizontalLayout();
	
	private Button okButton = new Button("Ok");
	private Button cancelButton = new Button("Cancel");
	private HorizontalLayout buttonsLayout = new HorizontalLayout();
	
	private ComboBox vmsComboBox = new ComboBox();
	
	public AddAppServerWindow(MonitoringView view, VirtualMachineRepository vmRepository, AppServerRepository appServerRepository){
		this.vmRepository = vmRepository;
		this.appServerRepository = appServerRepository;
		this.view = view;
		
		initWindow();		
		initComponents();
		initLayout();
		addListeners();
		
	}



	private void initWindow() {
		setCaption("Add Application Server");		
		setHeight(200, Unit.PIXELS);
		setWidth(300, Unit.PIXELS);
		setModal(true);
		setResizable(false);
	}
	
	private void initComponents() {
		for(VirtualMachine vm : vmRepository.findAll()){
			vmsComboBox.addItem(vm);
			if(vmsComboBox.getValue() == null)
				vmsComboBox.setValue(vm);
		}
		
		vmsComboBox.setNullSelectionAllowed(false);
		
	}

	private void addListeners() {
		
		okButton.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				
				AppServer a = new AppServer();
				VirtualMachine vm = (VirtualMachine) vmsComboBox.getValue();	
				if(vm == null){
					return;
				}
				a.setName(nameTextField.getValue());
				a.setJmxAdminPort(portTextField.getValue());
				a.setVirtualMachine(vm);
				appServerRepository.save(a);
				vm.getAppServers().add(a);
				vm.setIp(ipTextField.getValue());
				vmRepository.save(vm);
				
				close();;
				
			}
		});
		
		cancelButton.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();				
			}
		});
		
	}

	private void initLayout() {
		mainLayout.addComponent(vmsComboBox);
		nameLayout.setWidth(100, Unit.PERCENTAGE);
		nameLayout.addComponent(new Label("Name: "));
		nameLayout.addComponent(nameTextField);
		mainLayout.addComponent(nameLayout);
		
		portLayout.setWidth(100, Unit.PERCENTAGE);
		portLayout.addComponent(new Label("Jmx Admin Port: "));
		portLayout.addComponent(portTextField);
		mainLayout.addComponent(portLayout);
		
		ipLayout.setWidth(100, Unit.PERCENTAGE);
		ipLayout.addComponent(new Label("Ip: "));
		ipLayout.addComponent(ipTextField);
		mainLayout.addComponent(ipLayout);
		
		buttonsLayout.setWidth(100, Unit.PERCENTAGE);
		buttonsLayout.addComponent(okButton);
		buttonsLayout.addComponent(cancelButton);
		buttonsLayout.setComponentAlignment(okButton, Alignment.MIDDLE_LEFT);
		buttonsLayout.setComponentAlignment(cancelButton, Alignment.MIDDLE_RIGHT);
		mainLayout.addComponent(buttonsLayout);
		
		mainLayout.setSizeFull();
		mainLayout.setSpacing(true);
		mainLayout.setMargin(true);
		setContent(mainLayout);

	}

}
