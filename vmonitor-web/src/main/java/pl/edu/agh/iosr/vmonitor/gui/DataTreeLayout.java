package pl.edu.agh.iosr.vmonitor.gui;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import pl.edu.agh.iosr.vmonitor.data.db.AppServerRepository;
import pl.edu.agh.iosr.vmonitor.data.db.ApplicationRepository;
import pl.edu.agh.iosr.vmonitor.data.db.HypervisorRepository;
import pl.edu.agh.iosr.vmonitor.data.db.VirtualMachineRepository;
import pl.edu.agh.iosr.vmonitor.data.db.entity.AppServer;
import pl.edu.agh.iosr.vmonitor.data.db.entity.Application;
import pl.edu.agh.iosr.vmonitor.data.db.entity.Hypervisor;
import pl.edu.agh.iosr.vmonitor.data.db.entity.VirtualMachine;
import pl.edu.agh.iosr.vmonitor.jmx.JmxUtils;

import com.vaadin.data.util.HierarchicalContainer;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Tree;
import com.vaadin.ui.VerticalLayout;

@Component
@Scope("prototype")
public class DataTreeLayout extends VerticalLayout {
	
    @Autowired
    HypervisorRepository hypervisorRepository;

    @Autowired
    VirtualMachineRepository virtualMachineRepository;
    
    @Autowired
    AppServerRepository appServerRepository;
    
    @Autowired
    ApplicationRepository applicationRepository;
    
    
    private Tree tree;
    private HierarchicalContainer container;
    private Thread vmsThread;
    
    @PostConstruct
    public void initialize() {

        setWidth(100, Unit.PIXELS);
        setMargin(true);
        setSpacing(true);
        
        initComponents();
        initLayout();

    }
   

	private void initComponents(){
		container = new HierarchicalContainer();
		tree = new Tree("", container);
		tree.setImmediate(true);

		
		
        for(Hypervisor h : hypervisorRepository.findAll()){
        	addHypervisor(h);
        	
    		tree.expandItemsRecursively(h);
        }
		 	
    }
	
	public void refresh(){
		container.removeAllItems();
        for(Hypervisor h : hypervisorRepository.findAll()){
        	addHypervisor(h);
    	
    		tree.expandItemsRecursively(h);
        }
	}

	private void addHypervisor(Hypervisor h) {
		container.addItem(h);
		for(VirtualMachine vm : h.getVms()){
			container.addItem(vm);
			container.setParent(vm, h);
			
			for(AppServer as : vm.getAppServers()){
				container.addItem(as);
				container.setParent(as, vm);
				
				for(Application a : as.getApplications()){
					container.addItem(a);
					container.setParent(a, as);
					container.setChildrenAllowed(a, false);
				}
			}
		}
	}
	
	public void update(List<Hypervisor> hypervisors){

		for(Hypervisor h : getHypervisorsInTree())
			updateHypervisor(hypervisors,h);
			
	}
	
	private List<Hypervisor> getHypervisorsInTree(){
		List<Hypervisor> hypervisorsInTree = new ArrayList<>();
		for(Object o : container.getItemIds()){
			if(o instanceof Hypervisor)
				hypervisorsInTree.add((Hypervisor) o);
		}
		
		return hypervisorsInTree;
	}


	private void updateHypervisor(List<Hypervisor> hypervisors, Object o) {
		List<Application> toRemove = new ArrayList<>();
		Hypervisor hypervisor = (Hypervisor) o;
		for(Hypervisor h : hypervisors){
			
			
			
			if(h.getId() == hypervisor.getId()){
				Set<VirtualMachine> newVms = h.getVms();
				for(VirtualMachine vm : hypervisor.getVms()){
					for(VirtualMachine v : newVms){
						if(vm.getId() == v.getId())
							vm.setState(v.getState());
					}	
					if(!vm.getState().equals("Running")){
						container.setChildrenAllowed(vm, false);
					}else{
						container.setChildrenAllowed(vm, true);
						for(AppServer as : vm.getAppServers()){
							tree.setItemIcon(as, null);
							container.setChildrenAllowed(as, true);
							try{
								List<String> apps = JmxUtils.getApplications(vm.getIp(), as.getJmxAdminPort());
								
								if(container.getChildren(as) != null){
									for(Object o1: container.getChildren(as)){
										Application a = (Application) o1;
										if(!apps.contains(a.getName()))
											toRemove.add(a);
									}
								}
								
								List<Application> appsInTree = getApps(as);
								for(String name : apps){
									if(!containsApp(appsInTree, name)){
										addApplication(as, name);
									}
								}

								
							}catch(Exception e){
								e.printStackTrace();
								tree.setItemIcon(as, new ThemeResource("error.png"));
								container.setChildrenAllowed(as, false);
							}

						}
					}

				}
				break;
			}
		}
		removeOldApplications(toRemove);
	}


	private void addApplication(AppServer as, String name) {
		Application a = new Application();
		a.setName(name);
		a.setAppServer(as);
		as.getApplications().add(a);
		applicationRepository.save(a);
		appServerRepository.save(as);
		container.addItem(a);
		container.setParent(a, as);
		container.setChildrenAllowed(a, false);
	}
	
	private void removeOldApplications(List<Application> toRemove) {
				
		Iterator<Application> it = toRemove.iterator();
		while(it.hasNext()){
			Application ap = it.next();
			container.removeItem(ap);
			try{
				ap = applicationRepository.findOne(ap.getId());
				applicationRepository.delete(ap);
			}catch(Exception e){
				e.printStackTrace();
			}

		}

	}
	
	private boolean containsApp(List<Application> apps, String name){
		
		for(Application app : apps){
			if(app.getName().equals(name))
				return true;
		}
		
		return false;
	}
	
	private List<Application> getApps(AppServer as){
		
		List<Application> apps = new ArrayList<>();
		if(container.getChildren(as) != null)
			for(Object o : container.getChildren(as))
				apps.add((Application)o);
			
		
		return apps;
	}
	
    private void initLayout() {
    	addComponent(tree);
		
	}

	public Tree getTree() {
		return tree;
	}
	

}
