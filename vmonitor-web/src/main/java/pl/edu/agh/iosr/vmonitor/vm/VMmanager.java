package pl.edu.agh.iosr.vmonitor.vm;

import java.util.List;

import org.virtualbox_4_3.IMachine;
import org.virtualbox_4_3.IPerformanceCollector;
import org.virtualbox_4_3.IUnknown;
import org.virtualbox_4_3.IVirtualBox;
import org.virtualbox_4_3.VirtualBoxManager;

public class VMmanager {
	
	private VirtualBoxManager mgr ;
	
	public void connect(String ip, String port, String user, String pass){
		 mgr = VirtualBoxManager.createInstance(null);
	     mgr.connect("http://localhost:18083", user, pass);
	}
	
	public void disconnect(){
	    mgr.disconnect();
        mgr.cleanup();
	}

    public IPerformanceCollector getPerformanceCollector(){
        return mgr.getVBox().getPerformanceCollector();
    }
	
	public List<IMachine> getVirtualMachines(){      
        IVirtualBox vBox = mgr.getVBox();

        return vBox.getMachines();

	}

    public IUnknown getHost() {
        return mgr.getVBox().getHost();
    }

}
