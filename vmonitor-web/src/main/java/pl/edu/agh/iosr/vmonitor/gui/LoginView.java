package pl.edu.agh.iosr.vmonitor.gui;

import javax.annotation.PostConstruct;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.event.ShortcutListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.VaadinService;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Label;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

@Component
public class LoginView extends VerticalLayout implements View{
	
	@Autowired
	MonitoringView allHypervisorsView;
	
	private VerticalLayout mainLayout;
	
	private TextField username;
	private PasswordField password;
	private Button button;
	private Label invalidPassword;


    @PostConstruct
    public void initialize() {
    	

        setSizeFull();
        setMargin(true);
		
		initcomponents();		
		initLayout();
		addListeners();


    }

	private void addListeners() {
		button.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				Subject currentUser = SecurityUtils.getSubject();
				UsernamePasswordToken token = new UsernamePasswordToken(
						username.getValue(), password.getValue());
				try {
					currentUser.login(token);
					getUI().getNavigator().addView("monitoring", allHypervisorsView);
					getUI().getNavigator().navigateTo("monitoring");
					VaadinService
							.reinitializeSession(VaadinService.getCurrentRequest());
				} catch (Exception e) {
					username.setValue("");
					password.setValue("");
					invalidPassword.setVisible(true);
				}
					        	
			}
		});

		
	}

	private void initLayout() {
		addComponent(mainLayout);
		
		mainLayout.addComponent(invalidPassword);
		mainLayout.addComponent(username);
		mainLayout.addComponent(password);
		mainLayout.addComponent(button);
		
		
		mainLayout.setComponentAlignment(invalidPassword,Alignment.MIDDLE_CENTER);
		mainLayout.setComponentAlignment(username,Alignment.MIDDLE_CENTER);
		mainLayout.setComponentAlignment(password,Alignment.MIDDLE_CENTER);
		mainLayout.setComponentAlignment(button,Alignment.MIDDLE_CENTER);
		setComponentAlignment(mainLayout,Alignment.TOP_CENTER);
	}

	private void initcomponents() {
		mainLayout = new VerticalLayout();
		mainLayout.setSpacing(true);
		
		invalidPassword = new Label("<b>Invalid username or password!<b>", ContentMode.HTML);
		invalidPassword.setVisible(false);
		invalidPassword.setSizeUndefined();
		
		
		username = new TextField();
		username.setCaption("Login");
		username.focus();
		password = new PasswordField();
		password.setCaption("Password");
		
		button = new Button("Login");
		button.setClickShortcut(KeyCode.ENTER);

	}


	@Override
	public void enter(ViewChangeEvent event) {
		username.setValue("");
		password.setValue("");
		invalidPassword.setVisible(false);
		
	}

}
