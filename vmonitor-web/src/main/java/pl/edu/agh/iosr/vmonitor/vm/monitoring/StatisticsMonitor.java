package pl.edu.agh.iosr.vmonitor.vm.monitoring;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.virtualbox_4_3.Holder;
import org.virtualbox_4_3.IPerformanceCollector;
import org.virtualbox_4_3.IUnknown;

import pl.edu.agh.iosr.vmonitor.vm.VMmanager;

import com.vaadin.addon.charts.model.DataSeries;
import com.vaadin.addon.charts.model.DataSeriesItem;
import com.vaadin.ui.UI;

public class StatisticsMonitor implements Runnable {
    private static final String METRIC_RAM_USAGE_USED = "RAM/Usage/Used";
    private static final String METRIC_RAM_USAGE_TOTAL = "RAM/Usage/Total";
    public static final String METRIC_CPU_LOAD_USER = "CPU/Load/User";

    private static final String METRIC_BASE_CPU_LOAD = "CPU/Load";
    private static final String METRIC_BASE_RAM_USAGE = "RAM/Usage";

    private final List<String> metricNames;
    private final List<IUnknown> objects;
    private final IPerformanceCollector performanceCollector;
    private DataSeries dataSeries;

    private DataSeries ramDataSeries;

    public StatisticsMonitor(VMmanager manager, DataSeries cpuDataSeries, DataSeries ramDataSeries) {
        metricNames = new ArrayList<>();
        metricNames.add(METRIC_BASE_CPU_LOAD);
        metricNames.add(METRIC_RAM_USAGE_TOTAL);

        objects = new ArrayList<>();
        objects.add(manager.getHost());

        performanceCollector = manager.getPerformanceCollector();
        performanceCollector.setupMetrics(null, objects, 2L, 1L);

        this.dataSeries = cpuDataSeries;
        this.ramDataSeries = ramDataSeries;
    }

    @Override
    public void run() {
        while (true) {

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            Holder<List<String>> returnMetricNames = new Holder<>();
            Holder<List<IUnknown>> returnObjects = new Holder<>();
            Holder<List<String>> returnUnits = new Holder<>();
            Holder<List<Long>> returnScales = new Holder<>();
            Holder<List<Long>> returnSequenceNumbers = new Holder<>();
            Holder<List<Long>> returnDataIndices = new Holder<>();
            Holder<List<Long>> returnDataLengths = new Holder<>();

            metricNames.clear();
            metricNames.add(METRIC_CPU_LOAD_USER);
            metricNames.add(METRIC_RAM_USAGE_TOTAL);

            List<Integer> returnData = performanceCollector.queryMetricsData(null, objects, returnMetricNames,
                    returnObjects, returnUnits, returnScales, returnSequenceNumbers, returnDataIndices,
                    returnDataLengths);

            final Map<String, Double> processedData = new HashMap<>();

            for (int i = 0; i < returnMetricNames.value.size(); i++) {
                String metricName = returnMetricNames.value.get(i);
                int returnDataLength = returnDataLengths.value.get(i).intValue();
                if (returnDataLength > 0) {
                    int startIndex = returnDataIndices.value.get(i).intValue();
                    String unit = returnUnits.value.get(i);
                    long scale = returnScales.value.get(i);
                    double point = returnData.get(startIndex) / scale;
                    processedData.put(metricName, point);
                }
            }
            UI.getCurrent().access(new Runnable() {
                @Override
                public void run() {
                    Double cpuLoad = processedData.get(METRIC_CPU_LOAD_USER);
                    if (cpuLoad != null) {
                        dataSeries.add(new DataSeriesItem(new Date(), cpuLoad));
                    }
                    Double ramUsage = processedData.get(METRIC_RAM_USAGE_USED);
                    if (ramUsage != null) {
                        ramDataSeries.add(new DataSeriesItem(new Date(), ramUsage));
                    }
                }
            });
        }
    }
}
