package pl.edu.agh.iosr.vmonitor;

import javax.servlet.annotation.WebServlet;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import pl.edu.agh.iosr.vmonitor.gui.LoginView;
import pl.edu.agh.iosr.vmonitor.gui.MonitoringView;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.UI;

@Theme("mytheme")
@SuppressWarnings("serial")
public class MainUI extends UI implements ViewChangeListener
{
	
	

    @WebServlet(value = "/*", asyncSupported = true)
    @VaadinServletConfiguration(productionMode = false, ui = MainUI.class, widgetset = "pl.edu.agh.iosr.vmonitor.AppWidgetSet")
    public static class Servlet extends VaadinServlet {
    }

    @Override
    protected void init(VaadinRequest request) {
        
        WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(VaadinServlet.getCurrent().getServletContext());

        MonitoringView mainView = ctx.getBean(MonitoringView.class);
        LoginView loginView = ctx.getBean(LoginView.class);
        mainView.setLoginView(loginView);

        Navigator navigator = new Navigator(UI.getCurrent(), this);
        
        navigator.addView("", loginView);
        if(SecurityUtils.getSubject().isAuthenticated())
        	navigator.addView("monitoring", mainView);

        setPollInterval(2000);

    }

	@Override
	public boolean beforeViewChange(ViewChangeEvent event) {
		Subject currentUser = SecurityUtils.getSubject();
		if (currentUser.isAuthenticated() && event.getViewName().equals("")) {
			event.getNavigator().navigateTo("monitoring");
			return false;
		}

		if (!currentUser.isAuthenticated() && !event.getViewName().equals("")) {
			event.getNavigator().navigateTo("");
			return false;
		}

		return true;
	}

	@Override
	public void afterViewChange(ViewChangeEvent event) {
		// TODO Auto-generated method stub
		
	}

}
