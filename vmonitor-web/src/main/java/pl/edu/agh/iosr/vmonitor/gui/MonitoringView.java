package pl.edu.agh.iosr.vmonitor.gui;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Component;

import pl.edu.agh.iosr.vmonitor.data.db.AppServerRepository;
import pl.edu.agh.iosr.vmonitor.data.db.ApplicationRepository;
import pl.edu.agh.iosr.vmonitor.data.db.HypervisorRepository;
import pl.edu.agh.iosr.vmonitor.data.db.VirtualMachineRepository;
import pl.edu.agh.iosr.vmonitor.data.db.entity.AppServer;
import pl.edu.agh.iosr.vmonitor.data.db.entity.Application;
import pl.edu.agh.iosr.vmonitor.data.db.entity.Hypervisor;
import pl.edu.agh.iosr.vmonitor.data.db.entity.VirtualMachine;
import pl.edu.agh.iosr.vmonitor.data.stats.HypervisorTickRepository;
import pl.edu.agh.iosr.vmonitor.data.stats.VMachineTickRepository;
import pl.edu.agh.iosr.vmonitor.data.stats.VmStateTickRepository;
import pl.edu.agh.iosr.vmonitor.vm.monitoring.VmsStateMonitor;

import com.vaadin.data.Container;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@Component
public class MonitoringView extends VerticalLayout implements View {

    private static final long serialVersionUID = 1L;
    
    @Autowired
    HypervisorRepository hypervisorRepository;

    @Autowired
    VirtualMachineRepository virtualMachineRepository;
    
    @Autowired
    AppServerRepository appServerRepository;
    
    @Autowired
    ApplicationRepository applicationRepository;
    
    @Autowired
    DataTreeLayout dataTree;

    @Autowired
    private MongoOperations mongoOperations;

    @Autowired
    private HypervisorTickRepository hypervisorTickRepository;

    @Autowired
    private MachineStatsLayout machineStatsLayout;

    private LoginView loginView;
    
    private VerticalLayout mainLayout = new VerticalLayout();
    private HorizontalLayout horizontalLayout = new HorizontalLayout();
    private VerticalLayout rightLayout = new VerticalLayout();
    private Label label;
    private MenuBar menu;

    private Button button = new Button("Add Hypervisor");
    private InfoLayout info = new InfoLayout();
    private HypervisorStatsLayout hypervisorStatsLayout = new HypervisorStatsLayout();
    private Thread vmsThread;

    
    @PostConstruct
    public void initialize() {

        setSizeFull();
        setSpacing(true);
        
        initComponents();
        initLayout();
        addListeners();

    }


    @Override
    public void enter(ViewChangeEvent event) {
        refresh();

        if (vmsThread == null) {
            vmsThread = new Thread(
            		new VmsStateMonitor(hypervisorRepository, virtualMachineRepository, 
            				appServerRepository, applicationRepository, dataTree));
            vmsThread.start();
         }

    }


	public void refresh() {
		String login = (String) SecurityUtils.getSubject().getPrincipal();
		label.setValue("Logged as: <b>" + login + "</b>");
		
		Set<Hypervisor> hypervisors = new HashSet<>();
        for(Hypervisor h : hypervisorRepository.findAll())
            	hypervisors.add(h);

	}
    
	private void initComponents() {
		menu = new MenuBar();
		menu.setWidth(100, Unit.PERCENTAGE);
		
		MenuItem options = menu.addItem("Options", null);
		MenuItem newOption = options.addItem("New", null);
		MenuItem newHypervisor = newOption.addItem("Hypervisor", new Command() {
			
			@Override
			public void menuSelected(MenuItem selectedItem) {
				
				UI.getCurrent().addWindow(new AddHypervisorWindow(MonitoringView.this, hypervisorRepository));
				
			}
		});
		
		MenuItem newAppServer = newOption.addItem("AppServer", new Command() {
			
			@Override
			public void menuSelected(MenuItem selectedItem) {
				UI.getCurrent().addWindow(new AddAppServerWindow(MonitoringView.this, virtualMachineRepository, appServerRepository));
				
			}
		});
		
		MenuItem logout = menu.addItem("Logout", new Command() {
			
			@Override
			public void menuSelected(MenuItem selectedItem) {
				getUI().getNavigator().addView("", loginView);
				getUI().getNavigator().navigateTo("");
				SecurityUtils.getSubject().logout();					
				
			}
		});
		
		label = new Label("Logged as: ", ContentMode.HTML);
		label.setSizeUndefined();
		
		hypervisorStatsLayout.setVisible(false);
        hypervisorStatsLayout.setMongoOperations(mongoOperations);
        hypervisorStatsLayout.setHypervisorTickRepository(hypervisorTickRepository);

        machineStatsLayout.setVisible(false);

	}
	
	private void initLayout(){
		
		addComponent(menu);
		addComponent(mainLayout);
		
		mainLayout.setSizeFull();
		mainLayout.setWidth(100, Unit.PERCENTAGE);
		mainLayout.setMargin(true);
		mainLayout.setSpacing(true);
		mainLayout.addComponent(label);
		mainLayout.addComponent(horizontalLayout);
		mainLayout.setComponentAlignment(label, Alignment.TOP_LEFT);
	
		
		horizontalLayout.setSizeFull();
		horizontalLayout.addComponent(dataTree);
		horizontalLayout.addComponent(rightLayout);
    	horizontalLayout.setComponentAlignment(dataTree, Alignment.TOP_LEFT);
    	horizontalLayout.setExpandRatio(rightLayout, 0.75f);
    	horizontalLayout.setExpandRatio(dataTree, 0.25f);
		rightLayout.setSizeFull();
		rightLayout.addComponent(info);
		rightLayout.addComponent(hypervisorStatsLayout);
		rightLayout.setExpandRatio(hypervisorStatsLayout, 1f);
        rightLayout.addComponent(machineStatsLayout);
        rightLayout.setExpandRatio(machineStatsLayout, 1f);
		
		
		setExpandRatio(mainLayout, 0.9f);
		mainLayout.setExpandRatio(horizontalLayout, 0.9f);
		
		
	}
    
    private void addListeners() {
    	final AddHypervisorWindow window = new AddHypervisorWindow(this, hypervisorRepository);
        button.addClickListener(new Button.ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
            	UI.getCurrent().addWindow(window);

            }
        });
        
        dataTree.getTree().addItemClickListener(new ItemClickListener() {
			
			@Override
			public void itemClick(ItemClickEvent event) {
				Object item = event.getItemId();
                hypervisorStatsLayout.setVisible(false);
                machineStatsLayout.setVisible(false);
				if(item instanceof Hypervisor){
					info.update((Hypervisor) item);
//					hypervisorStatsLayout.startThreads((Hypervisor) item);
                    hypervisorStatsLayout.setMonitoredHypervisor((Hypervisor) item);
					hypervisorStatsLayout.setVisible(true);
					
				}else if(item instanceof VirtualMachine){
					info.update((VirtualMachine) item);
                    machineStatsLayout.setMonitoredMachine((VirtualMachine) item);
                    machineStatsLayout.setVisible(true);

				}else if(item instanceof AppServer){
					
				}else if(item instanceof Application){
					
				}
				
			}
		});

    }


	public LoginView getLoginView() {
		return loginView;
	}


	public void setLoginView(LoginView loginView) {
		this.loginView = loginView;
	}



    
    

}
