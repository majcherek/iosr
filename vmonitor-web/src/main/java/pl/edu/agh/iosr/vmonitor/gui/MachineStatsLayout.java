package pl.edu.agh.iosr.vmonitor.gui;

import com.vaadin.addon.charts.Chart;
import com.vaadin.addon.charts.model.*;
import com.vaadin.data.Property;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Component;
import pl.edu.agh.iosr.vmonitor.data.db.entity.Hypervisor;
import pl.edu.agh.iosr.vmonitor.data.db.entity.VirtualMachine;
import pl.edu.agh.iosr.vmonitor.data.stats.*;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.List;

@Component
public class MachineStatsLayout extends VerticalLayout {

    private VerticalLayout stateChartContainer;

    //For aggregation result
    private static class StateCount {
        private String id;
        private Long count;
    }

    @Autowired
    private MongoOperations mongoOperations;

    @Autowired
    private VMachineTickRepository vMachineTickRepository;

    @Autowired
    private VmStateTickRepository vmStateTickRepository;

    private static final long serialVersionUID = 7630851737458863812L;
    private VerticalLayout chartContainer;

    private ComboBox metricCombo;
    private DateField fromDate;
    private DateField toDate;

    public MachineStatsLayout() {
    }

    @PostConstruct
    private void initComponents() {

        stateChartContainer = new VerticalLayout();
        stateChartContainer.setMargin(true);
        addComponent(stateChartContainer);

        HorizontalLayout metricProperties = new HorizontalLayout();
        metricProperties.setMargin(true);
        addComponent(metricProperties);

        //metric name chooser
        metricCombo = new ComboBox("Choose metric:");
        metricProperties.addComponent(metricCombo);
        List<String> allMetricNames = mongoOperations.getCollection("vMachineTick").distinct(VMachineTick.METRIC_NAME__FIELD);
        for (String metricName : allMetricNames) {
            metricCombo.addItem(metricName);
        }

        //selection of date
        fromDate = new DateField("From", new Date());
        fromDate.setResolution(Resolution.MINUTE);
        toDate = new DateField("To", new Date());
        toDate.setResolution(Resolution.MINUTE);

        metricProperties.addComponent(fromDate);
        metricProperties.addComponent(toDate);

        chartContainer = new VerticalLayout();
        chartContainer.setMargin(true);
        addComponent(chartContainer);

    }

    public void setMonitoredMachine(final VirtualMachine machine) {

        Aggregation aggregation = Aggregation.newAggregation(
                Aggregation.match(Criteria.where(VmStateTick.MACHINE_ID__FIELD).is(machine.getVirtualboxId())),
                Aggregation.group(VmStateTick.STATE__FIELD).count().as("count")
        );

        List<StateCount> aggregationResult = mongoOperations.aggregate(aggregation, VmStateTick.class, StateCount.class).getMappedResults();

        refreshStateChart(aggregationResult);

        chartContainer.removeAllComponents();

        Property.ValueChangeListener listener = new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                refreshChart(machine.getVirtualboxId());
            }
        };

        metricCombo.addValueChangeListener(listener);
        toDate.addValueChangeListener(listener);
        fromDate.addValueChangeListener(listener);
    }

    private void refreshStateChart(List<StateCount> aggregationResult) {
        stateChartContainer.removeAllComponents();
        if (aggregationResult != null && aggregationResult.size()>0) {
            DataSeries series = new DataSeries();
            for (StateCount stateCount : aggregationResult) {
                series.add(new DataSeriesItem(stateCount.id,stateCount.count));
            }
            Chart stateChart = new Chart(ChartType.PIE);
            stateChart.getConfiguration().setTitle("State statistics");
            stateChart.getConfiguration().setSeries(series);
            stateChartContainer.addComponent(stateChart);
        }
    }

    private void refreshChart(String id) {
        chartContainer.removeAllComponents();
        String metricName = (String) metricCombo.getValue();
        if (metricName != null) {
            Chart metricChart = new Chart(ChartType.LINE);
            metricChart.getConfiguration().setTitle(metricName);
            DataSeries series = new DataSeries(metricName);

            PlotOptionsLine plotOptions = new PlotOptionsLine();
            plotOptions.setMarker(new Marker(false));
            metricChart.getConfiguration().setPlotOptions(plotOptions);

            Date startDate = fromDate.getValue();
            Date endDate = toDate.getValue();

            if (vMachineTickRepository != null) {

                List<VMachineTick> queryResult = vMachineTickRepository.findByMachineIdAndMetricNameAndDateBetween(id, metricName, startDate, endDate,
                        new Sort(Sort.Direction.ASC, VMachineTick.DATE__FIELD));

                for (VMachineTick tick : queryResult) {
                    series.add(new DataSeriesItem(tick.getDate(), tick.getValue()));
                }

                metricChart.getConfiguration().getxAxis().setType(AxisType.DATETIME);

                metricChart.getConfiguration().addSeries(series);
                chartContainer.addComponent(metricChart);
            }
        }
    }


}
