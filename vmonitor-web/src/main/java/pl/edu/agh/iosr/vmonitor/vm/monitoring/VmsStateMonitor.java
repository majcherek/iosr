package pl.edu.agh.iosr.vmonitor.vm.monitoring;

import java.util.ArrayList;
import java.util.List;

import org.virtualbox_4_3.IMachine;

import pl.edu.agh.iosr.vmonitor.data.db.AppServerRepository;
import pl.edu.agh.iosr.vmonitor.data.db.ApplicationRepository;
import pl.edu.agh.iosr.vmonitor.data.db.HypervisorRepository;
import pl.edu.agh.iosr.vmonitor.data.db.VirtualMachineRepository;
import pl.edu.agh.iosr.vmonitor.data.db.entity.AppServer;
import pl.edu.agh.iosr.vmonitor.data.db.entity.Application;
import pl.edu.agh.iosr.vmonitor.data.db.entity.Hypervisor;
import pl.edu.agh.iosr.vmonitor.data.db.entity.VirtualMachine;
import pl.edu.agh.iosr.vmonitor.gui.DataTreeLayout;
import pl.edu.agh.iosr.vmonitor.jmx.JmxUtils;
import pl.edu.agh.iosr.vmonitor.vm.VMmanager;

import com.vaadin.ui.UI;

public class VmsStateMonitor implements Runnable {
	
    private HypervisorRepository hypervisorRepository;
    private VirtualMachineRepository virtualMachineRepository;
    private AppServerRepository appServerRepository;
    private ApplicationRepository applicationRepository;

	private DataTreeLayout dataTreeLayout;

	private List<Hypervisor> hypervisors = new ArrayList<>();
	
	public VmsStateMonitor(HypervisorRepository hypervisorRepository, 
			VirtualMachineRepository virtualMachineRepository, 
			AppServerRepository appServerRepository, ApplicationRepository applicationRepository, DataTreeLayout dataTreeLayout){
		this.dataTreeLayout = dataTreeLayout;
		this.hypervisorRepository = hypervisorRepository;
		this.virtualMachineRepository = virtualMachineRepository;
		this.appServerRepository = appServerRepository;
		this.applicationRepository = applicationRepository;


		
		for(Hypervisor h : hypervisorRepository.findAll()){
			hypervisors.add(h);
			
			
		}
		
	}

	@Override
	public void run() {
		
        while (true) {
        	
        	try{
            	
            	for(Hypervisor h : hypervisors){
            		VMmanager manager = new VMmanager();
            		  manager.connect(h.getIp(),h.getPort(),h.getUser(),h.getPass());
                    List<IMachine> machines = manager.getVirtualMachines();

                    for (IMachine m : machines) {

                        VirtualMachine vm = virtualMachineRepository.findByVirtualboxId(m.getId());
                        if (vm == null) 
                            addNewVm(h, m);
                        else {
                    		if(vm.getState() != m.getState().name()){
                    			updateVm(h, m, vm);
                    		}
                        }
                    }      	
                    manager.disconnect();
            	}
            	
            	for(AppServer as : appServerRepository.findAll()){
            		if(as.getVirtualMachine() != null){
            			List<String> appNames = JmxUtils.getApplications(as.getVirtualMachine().getIp(), as.getJmxAdminPort());
                		
                		List<Application> apps = new ArrayList<Application>();
                		for(Application a : as.getApplications())
                			apps.add(a);
                		
                		for(String name : appNames){
                			if(!containsApp(apps, name)){
                				Application application = new Application();
                				application.setName(name);
                				application.setAppServer(as);
                				
                				as.getApplications().add(application);
                				appServerRepository.save(as);
                			}
                				
                		}
                		
                		for(Application a : apps){
                			if(!appNames.contains(a.getName()))
                				applicationRepository.delete(a);
                		}
                		
            		}
            		
            	}


                UI.getCurrent().access(new Runnable() {
                    @Override
                    public void run() {
                    	dataTreeLayout.refresh();
                    	
                    }
                });
                


                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
        		
        	}catch(Exception e){
        		e.printStackTrace();
        	}
 
        }

	}
	
	private boolean containsApp(List<Application> apps, String name){
		
		for(Application app : apps){
			if(app.getName().equals(name))
				return true;
		}
		
		return false;
	}

	private void updateVm(Hypervisor h, IMachine m, VirtualMachine vm) {
		
		vm.setState(m.getState().name());
		vm.setOs(m.getOSTypeId());
		if (!h.getVms().contains(vm)) {
		    h.getVms().add(vm);
		}
		h = hypervisorRepository.save(h);
		virtualMachineRepository.save(vm);

	}

	private void addNewVm(Hypervisor h, IMachine m) {
		
		VirtualMachine vm;
		vm = new VirtualMachine();
		vm.setVirtualboxId(m.getId());
		vm.setName(m.getName());
		vm.setOs(m.getOSTypeId());
		vm.setState(m.getState().name());

		virtualMachineRepository.save(vm);

		h.getVms().add(vm);
		h = hypervisorRepository.save(h);
	}

}
