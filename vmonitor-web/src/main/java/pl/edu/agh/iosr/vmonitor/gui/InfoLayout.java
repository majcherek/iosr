package pl.edu.agh.iosr.vmonitor.gui;

import pl.edu.agh.iosr.vmonitor.data.db.entity.AppServer;
import pl.edu.agh.iosr.vmonitor.data.db.entity.Hypervisor;
import pl.edu.agh.iosr.vmonitor.data.db.entity.VirtualMachine;

import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

public class InfoLayout extends VerticalLayout{

	private static final long serialVersionUID = 1213008416299934014L;
	
	private Label id = new Label("", ContentMode.HTML);
	private Label ip = new Label("", ContentMode.HTML);
	private Label port = new Label("", ContentMode.HTML);
	
	private Label name = new Label("", ContentMode.HTML);
	private Label virtualboxId = new Label("", ContentMode.HTML);
	private Label state = new Label("", ContentMode.HTML);
	private Label os = new Label("", ContentMode.HTML);
	
	private Label jmxport = new Label("", ContentMode.HTML);
	
	public InfoLayout(){
		setSpacing(true);
	}
	
	
	public void update(Hypervisor h){
		removeAllComponents();

		id.setValue("<b>ID:</b> " + h.getId());
		ip.setValue("<b>IP:</b> " + h.getIp());
		port.setValue("<b>PORT:</b> " + h.getPort());
		
		addComponent(id);
		addComponent(ip);
		addComponent(port);

	}
	
	public void update(VirtualMachine vm){
		removeAllComponents();

		name.setValue("<b>NAME:</b> " + vm.getName());
		virtualboxId.setValue("<b>VIRTUALBOX ID:</b> " + vm.getVirtualboxId());
		state.setValue("<b>STATE:</b> " + vm.getState());
		os.setValue("<b>OS:</b> " + vm.getOs());
		ip.setValue("<b>IP:</b> " + vm.getIp());
		
		addComponent(name);
		addComponent(virtualboxId);
		addComponent(state);
		addComponent(os);
		addComponent(ip);

	}
	
	public void update(AppServer as){
		removeAllComponents();

		name.setValue("<b>NAME:</b> " + as.getName());
		jmxport.setValue("<b>JMX ADNMIN PORT:</b> " + as.getJmxAdminPort());

		addComponent(name);
		addComponent(jmxport);

	}

	
	

}
