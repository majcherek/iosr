package pl.edu.agh.iosr.vmonitor.db;

import com.mongodb.Mongo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoFactoryBean;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * Created by molok on 2014-05-24.
 */
@Configuration
@EnableMongoRepositories("pl.edu.agh.iosr.vmonitor.data.stats")
public class MongoConfig {

    @Autowired
    private Mongo mongo;

    @Bean
    public MongoFactoryBean mongo(){
        MongoFactoryBean mongoFactoryBean = new MongoFactoryBean();
        return mongoFactoryBean;
    }

    @Bean
    public MongoDbFactory dbFactory(){
        return new SimpleMongoDbFactory(mongo,"vmonitor");
    }

    @Bean
    public MongoTemplate mongoTemplate(){
        return new MongoTemplate(dbFactory());
    }


}
