package pl.edu.agh.iosr.vmonitor.gui;

import pl.edu.agh.iosr.vmonitor.data.db.HypervisorRepository;
import pl.edu.agh.iosr.vmonitor.data.db.entity.Hypervisor;

import com.vaadin.data.validator.AbstractValidator;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

public class AddHypervisorWindow extends Window {
	
	private HypervisorRepository hypervisorRepository;
	private MonitoringView view;
	
	private TextValidator validator = new TextValidator("Pole nie może być puste!");
	
	private VerticalLayout mainLayout = new VerticalLayout();
	 
	private TextField ipTextField = new TextField();
	private HorizontalLayout ipLayout = new HorizontalLayout();
	
	private TextField portTextField = new TextField();
	private HorizontalLayout portLayout = new HorizontalLayout();
	
	private TextField userTextField = new TextField();
	private HorizontalLayout userLayout = new HorizontalLayout();
	
	private PasswordField passwordTextField = new PasswordField();
	private HorizontalLayout passwordLayout = new HorizontalLayout();
	
	private Button okButton = new Button("Ok");
	private Button cancelButton = new Button("Cancel");
	private HorizontalLayout buttonsLayout = new HorizontalLayout();
	
	public AddHypervisorWindow(MonitoringView view, HypervisorRepository hypervisorRepository){
		this.hypervisorRepository = hypervisorRepository;
		this.view = view;
		
		initWindow();				
		addComponents();
		addListeners();
		
	}

	private void initWindow() {
		setCaption("Add Hypervisor");		
		setHeight(300, Unit.PIXELS);
		setWidth(300, Unit.PIXELS);
		setModal(true);
		setResizable(false);
	}

	private void addListeners() {
		
		okButton.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				
				ipTextField.validate();
				portTextField.validate();
				userTextField.validate();
				passwordTextField.validate();
				
				Hypervisor h = new Hypervisor();
				h.setIp(ipTextField.getValue());
				h.setPort(portTextField.getValue());
				h.setUser(userTextField.getValue());
				h.setPass(passwordTextField.getValue());
				hypervisorRepository.save(h);
				view.refresh();
				close();
				
			}
		});
		
		cancelButton.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				close();				
			}
		});
		
	}

	private void addComponents() {
		ipLayout.setWidth(100, Unit.PERCENTAGE);
		ipLayout.addComponent(new Label("Ip: "));
		ipLayout.addComponent(ipTextField);
		mainLayout.addComponent(ipLayout);

		
		portLayout.setWidth(100, Unit.PERCENTAGE);
		portLayout.addComponent(new Label("Port: "));
		portLayout.addComponent(portTextField);
		mainLayout.addComponent(portLayout);
		
		userLayout.setWidth(100, Unit.PERCENTAGE);
		userLayout.addComponent(new Label("User: "));
		userLayout.addComponent(userTextField);
		mainLayout.addComponent(userLayout);
		
		passwordLayout.setWidth(100, Unit.PERCENTAGE);
		passwordLayout.addComponent(new Label("Password: "));
		passwordLayout.addComponent(passwordTextField);
		mainLayout.addComponent(passwordLayout);
		
		buttonsLayout.setWidth(100, Unit.PERCENTAGE);
		buttonsLayout.addComponent(okButton);
		buttonsLayout.addComponent(cancelButton);
		buttonsLayout.setComponentAlignment(okButton, Alignment.MIDDLE_LEFT);
		buttonsLayout.setComponentAlignment(cancelButton, Alignment.MIDDLE_RIGHT);
		mainLayout.addComponent(buttonsLayout);
		
		mainLayout.setSizeFull();
		mainLayout.setSpacing(true);
		mainLayout.setMargin(true);
		setContent(mainLayout);
		
		ipTextField.addValidator(validator);
		portTextField.addValidator(validator);
		userTextField.addValidator(validator);
		passwordTextField.addValidator(validator);

	}
	
	class TextValidator extends AbstractValidator<String>{

		public TextValidator(String errorMessage) {
			super(errorMessage);
		}

		@Override
		protected boolean isValidValue(String value) {
			if(value.length() == 0)
				return false;
			return true;
		}

		@Override
		public Class<String> getType() {
			return String.class;
		}
		
	}

}
