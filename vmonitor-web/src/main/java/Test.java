import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.virtualbox_4_3.Holder;
import org.virtualbox_4_3.IHost;
import org.virtualbox_4_3.IMachine;
import org.virtualbox_4_3.IPerformanceCollector;
import org.virtualbox_4_3.IUnknown;
import org.virtualbox_4_3.IVirtualBox;
import org.virtualbox_4_3.VirtualBoxManager;

/**
 * Created by molok on 2014-03-26.
 */
public class Test {

    public static final String METRIC_CPU_LOAD_USER = "CPU/Load/User";

    public static void main(String[] args) throws IOException, InterruptedException {
        String user = "Gosia";
        String pass = "misior";

        VirtualBoxManager mgr = VirtualBoxManager.createInstance(null);
        mgr.connect("http://localhost:18083", user, pass);
        IVirtualBox vBox = mgr.getVBox();
        for (IMachine machine : vBox.getMachines()) {
            System.out.println(machine.getName());
            System.out.println(machine.getId());
        }

        IPerformanceCollector performanceCollector = vBox.getPerformanceCollector();
        List<String> metricNames = performanceCollector.getMetricNames();
        for (String name : metricNames) {
            System.out.println(name);
        }
        System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        IHost host = vBox.getHost();

        List<String> metrics = new ArrayList<>();
        metrics.add(METRIC_CPU_LOAD_USER);

        List<IUnknown> objects = new ArrayList<>();
        objects.add(host);

        performanceCollector.setupMetrics(null, objects, 1L, 10L);

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Holder<List<String>> returnMetricNames = new Holder<>();
        Holder<List<IUnknown>> returnObjects = new Holder<>();
        Holder<List<String>> returnUnits = new Holder<>();
        Holder<List<Long>> returnScales = new Holder<>();
        Holder<List<Long>> returnSequenceNumbers = new Holder<>();
        Holder<List<Long>> returnDataIndices = new Holder<>();
        Holder<List<Long>> returnDataLengths = new Holder<>();

        List<Integer> returnData = performanceCollector.queryMetricsData(null, objects, returnMetricNames,
                returnObjects, returnUnits, returnScales, returnSequenceNumbers, returnDataIndices, returnDataLengths);

        List<String> mNames = returnMetricNames.value;
        for (int i = 0; i < mNames.size(); ++i) {
            System.out.println("========================================");
            System.out.println(mNames.get(i));
            int dataLength = returnDataLengths.value.get(i).intValue();
            int startIndex = returnDataIndices.value.get(i).intValue();
            String unit = returnUnits.value.get(i);
            Long scale = returnScales.value.get(i);
            for (int j = 0; j < dataLength; ++j) {
                double point = returnData.get(startIndex + j)/scale;
                System.out.println(point+" "+unit);
            }
            System.out.println("========================================");
        }

        mgr.disconnect();
        mgr.cleanup();
    }

}
