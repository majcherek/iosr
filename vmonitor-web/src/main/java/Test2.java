import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.IntrospectionException;
import javax.management.MBeanException;
import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;



public class Test2 {
	
	private static final String SERVICE = "service:jmx:rmi:///jndi/rmi://10.15.0.2:8686/jmxrmi";
	private static final String APPLICATIONS_MBEAN_NAME = "amx:pp=/domain,type=applications";
	
    public static void main(String[] args) throws IOException, MalformedObjectNameException, InstanceNotFoundException, IntrospectionException, ReflectionException, AttributeNotFoundException, MBeanException {
    	
    	
        JMXServiceURL target = new JMXServiceURL(SERVICE);
        Map env = new HashMap();
		String[] creds = {"admin", "misior"};
		env.put(JMXConnector.CREDENTIALS, creds);
        JMXConnector connector = JMXConnectorFactory.connect(target, env);
        MBeanServerConnection serverConnection = connector.getMBeanServerConnection();
        
        ObjectName bean = new ObjectName(APPLICATIONS_MBEAN_NAME);
        
        List<ObjectName> apps = (List<ObjectName>) serverConnection.invoke(bean, "getApplications", null, null);
        for(ObjectName app : apps){
            System.out.println(serverConnection.getAttribute(app, "Name"));
        }
        
        connector.close();

    }

}
