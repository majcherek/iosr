package pl.edu.agh.iosr.vmonitor.web.test;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import pl.edu.agh.iosr.vmonitor.vm.VMmanager;


public class VirtualManagerTest {

		VMmanager manager;
		
	    @Before
	    public void setup(){
	    	manager = new VMmanager();
	    	manager.connect("localhost", "18083", "Gosia", "misior");
	    }


	    @Test
	    public void connectionTest(){
	    	Assert.assertNotNull(manager.getHost());
	    }
	    
	    @Test
	    public void gettingMachinesTest(){
	    	Assert.assertTrue(manager.getVirtualMachines().size() > 0);
	    }


	    @After
	    public void cleanUp(){
	    	manager.disconnect();
	    }


}
