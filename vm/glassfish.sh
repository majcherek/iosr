﻿#!/usr/bin/env bash
apt-get update
apt-get install -y openjdk-7-jdk
apt-get install -y unzip
wget -nc http://download.java.net/glassfish/4.0/release/glassfish-4.0.zip -O /vagrant/glassfish-4.0.zip
rm -rf /tmp/glassfish4
unzip /vagrant/glassfish-4.0.zip -d /tmp
#/tmp/glassfish4/glassfish/bin/asadmin -u admin -W /vagrant/change change-admin-password
/tmp/glassfish4/glassfish/bin/asadmin start-domain
#/tmp/glassfish4/glassfish/bin/asadmin -u admin -W /vagrant/pass enable-secure-admin
#/tmp/glassfish4/glassfish/bin/asadmin -u admin -W /vagrant/pass restart-domain