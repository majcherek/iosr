package pl.edu.agh.iosr.vmonitor.agent.config;

import org.neo4j.graphdb.GraphDatabaseService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.neo4j.config.EnableNeo4jRepositories;
import org.springframework.data.neo4j.config.Neo4jConfiguration;
import org.springframework.data.neo4j.rest.SpringRestGraphDatabase;

@Configuration
@EnableNeo4jRepositories("pl.edu.agh.iosr.vmonitor.data.db")
public class Neo4jConfig extends Neo4jConfiguration {
	
	public Neo4jConfig() {
		setBasePackage("pl.edu.agh.iosr.vmonitor.data.db.entity");
	}
	
    @Bean
    public GraphDatabaseService graphDatabaseService() {
       // GraphDatabaseService service = new GraphDatabaseFactory().newEmbeddedDatabase("/db/data");
       // return service;
    	
    	return new SpringRestGraphDatabase("http://localhost:7474/db/data");
    }

}
