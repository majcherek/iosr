package pl.edu.agh.iosr.vmonitor.agent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.virtualbox_4_3.*;
import pl.edu.agh.iosr.vmonitor.data.db.HypervisorRepository;
import pl.edu.agh.iosr.vmonitor.data.db.entity.Hypervisor;
import pl.edu.agh.iosr.vmonitor.data.db.entity.VirtualMachine;
import pl.edu.agh.iosr.vmonitor.data.stats.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.regex.Pattern;

@Component
public class InfrastractureMonitor {

    @Autowired
    private HypervisorRepository hypervisorRepository;

    @Autowired
    private HypervisorTickRepository hypervisorTickRepository;

    @Autowired
    private VMachineTickRepository machineTickRepository;

    @Autowired
    private ExecutorService executorService;

    @Autowired
    private VmStateTickRepository vmStateTickRepository;

    private Pattern badSuffix = Pattern.compile("^.*(:(max|min|avg))$");

    private void collectHypervisorData(Hypervisor hypervisor) {
        VirtualBoxManager manager = VirtualBoxManager.createInstance(null);
        manager.connect("http://" + hypervisor.getIp() + ":" + hypervisor.getPort(), hypervisor.getUser(), hypervisor.getPass());

        IVirtualBox virtualBox = manager.getVBox();
        IPerformanceCollector performanceCollector = virtualBox.getPerformanceCollector();

        Set<VirtualMachine> configuredMachines = hypervisor.getVms();
        List<IMachine> allMachines = virtualBox.getMachines();
        List<IMachine> monitoredMachines = new ArrayList<>();

        for (IMachine machine : allMachines) {
            for (VirtualMachine vMachine : configuredMachines) {
                if (vMachine.getVirtualboxId().equals(machine.getId())) {
                    monitoredMachines.add(machine);
                }
            }
        }

        List<IUnknown> objects = new ArrayList<>();

        objects.add(virtualBox.getHost());
        objects.addAll(monitoredMachines);

        performanceCollector.setupMetrics(null, objects, 1L, 1L);

        //Wait for data
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        collectHostData(performanceCollector, hypervisor, virtualBox.getHost());
        collectMachinesData(performanceCollector, monitoredMachines);

        manager.disconnect();
        manager.cleanup();

    }

    private void collectMachinesData(IPerformanceCollector performanceCollector, List<IMachine> monitoredMachines) {

        Holder<List<String>> returnMetricNames = new Holder<>();
        Holder<List<IUnknown>> returnObjects = new Holder<>();
        Holder<List<String>> returnUnits = new Holder<>();
        Holder<List<Long>> returnScales = new Holder<>();
        Holder<List<Long>> returnSequenceNumbers = new Holder<>();
        Holder<List<Long>> returnDataIndices = new Holder<>();
        Holder<List<Long>> returnDataLengths = new Holder<>();

        List<IUnknown> objects = new ArrayList<>();

        for (int m = 0; m < monitoredMachines.size(); ++m) {

            IMachine machine = monitoredMachines.get(m);

            VmStateTick stateTick = new VmStateTick();
            stateTick.setDate(new Date());
            stateTick.setMachineId(machine.getId());
            stateTick.setState(machine.getState());

            vmStateTickRepository.save(stateTick);

            objects.clear();

            objects.add(machine);

            List<Integer> returnData = performanceCollector.queryMetricsData(null, objects, returnMetricNames,
                    returnObjects, returnUnits, returnScales, returnSequenceNumbers, returnDataIndices,
                    returnDataLengths);

            for (int i = 0; i < returnMetricNames.value.size(); i++) {
                String metricName = returnMetricNames.value.get(i);
                if (!badSuffix.matcher(metricName).matches()) {
                    int returnDataLength = returnDataLengths.value.get(i).intValue();
                    if (returnDataLength > 0) {
                        int startIndex = returnDataIndices.value.get(i).intValue();
                        String unit = returnUnits.value.get(i);
                        long scale = returnScales.value.get(i);
                        double point = returnData.get(startIndex) / scale;
                        VMachineTick tick = new VMachineTick();
                        tick.setUnit(unit);
                        tick.setValue(point);
                        tick.setMetricName(metricName);
                        tick.setMachineId(machine.getId());
                        tick.setDate(new Date());
                        machineTickRepository.save(tick);
                    }
                }
            }
        }

    }

    private void collectHostData(IPerformanceCollector performanceCollector, Hypervisor hypervisor, IHost host) {

        Holder<List<String>> returnMetricNames = new Holder<>();
        Holder<List<IUnknown>> returnObjects = new Holder<>();
        Holder<List<String>> returnUnits = new Holder<>();
        Holder<List<Long>> returnScales = new Holder<>();
        Holder<List<Long>> returnSequenceNumbers = new Holder<>();
        Holder<List<Long>> returnDataIndices = new Holder<>();
        Holder<List<Long>> returnDataLengths = new Holder<>();

        List<IUnknown> hostList = new ArrayList<>();
        hostList.add(host);

        //collect host data
        List<Integer> returnData = performanceCollector.queryMetricsData(null, hostList, returnMetricNames,
                returnObjects, returnUnits, returnScales, returnSequenceNumbers, returnDataIndices,
                returnDataLengths);

        for (int i = 0; i < returnMetricNames.value.size(); i++) {
            String metricName = returnMetricNames.value.get(i);
            if (!badSuffix.matcher(metricName).matches()) {
                int returnDataLength = returnDataLengths.value.get(i).intValue();
                if (returnDataLength > 0) {
                    int startIndex = returnDataIndices.value.get(i).intValue();
                    String unit = returnUnits.value.get(i);
                    long scale = returnScales.value.get(i);
                    double point = returnData.get(startIndex) / scale;
                    HypervisorTick tick = new HypervisorTick();
                    tick.setUnit(unit);
                    tick.setValue(point);
                    tick.setMetricName(metricName);
                    tick.setHypervisorId(hypervisor.getId());
                    tick.setDate(new Date());
                    hypervisorTickRepository.save(tick);
                }
            }
        }
    }

    @Scheduled(fixedDelay = 5000)
    public void collectData() {

        List<Hypervisor> allHypervisor = hypervisorRepository.findAll().as(List.class);
        for (final Hypervisor hypervisor : allHypervisor) {
            executorService.submit(new Runnable() {
                @Override
                public void run() {
                    collectHypervisorData(hypervisor);
                }
            });
        }

    }

}
